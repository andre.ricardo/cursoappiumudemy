package br.qa.andrericardo.appium.test;

import org.junit.Assert;
import org.junit.Test;

import br.qa.andrericardo.appium.core.BaseTest;
import br.qa.andrericardo.appium.page.AbasPage;
import br.qa.andrericardo.appium.page.MenuPage;

public class AbasTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private AbasPage abas = new AbasPage();
	@Test
	public void deveInteregirComAbas() {
		
		//acessar menu abas
		menu.acessarAbas();
		
		//verificar que esta na aba 1
		Assert.assertTrue(abas.isAba1());
		
		//acessar aba 2
		abas.selecionarAba2();

		//verificar que esta na aba 2
		Assert.assertTrue(abas.isAba2());
	}

}
