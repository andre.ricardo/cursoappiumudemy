package br.qa.andrericardo.appium.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.qa.andrericardo.appium.core.BaseTest;
import br.qa.andrericardo.appium.page.AlertaPage;
import br.qa.andrericardo.appium.page.MenuPage;

public class AlertaTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage(); 
	private AlertaPage alerta = new AlertaPage();
	
	@Before
	public void setup() {
		//acessar menu alerta
		menu.acessarAlertas();
	}
	
	@Test
	public void deveConfirmarAlerta() {
		//clicar em alerta confirm
		alerta.clicarAlertaConfirm();
		
		//verificar os textos
		Assert.assertEquals("Info", alerta.obterTituloAlerta());
		Assert.assertEquals("Confirma a opera��o?", alerta.obterMensagemAlerta());
		
		//confirmar alerta
		alerta.confirmar();
		
		//verificar nova mensagem 
		Assert.assertEquals("Confirmado", alerta.obterMensagemAlerta());
		
		//sair
		alerta.sair();
	}
	
	@Test
	public void deveClicarForaAlerta() {
		//clicar alerta simples
		alerta.clicarAlertaSimples();
		
		//clicar fora da caixa
		alerta.clicarForaCaixa();
		
		//verificar que a mensagem n�o est� presente
		Assert.assertFalse(alerta.existeElementoPorTexto("Pode clicar no OK ou fora da caixa para sair"));
//		Assert.assertTrue(alerta.existeElementoPorTexto("ALERTA SIMPLES", 
//				"ALERTA RESTRITIVO",
//				"ALERTA CONFIRM"));
	}

}