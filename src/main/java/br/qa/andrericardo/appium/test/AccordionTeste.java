package br.qa.andrericardo.appium.test;

import org.junit.Assert;
import org.junit.Test;

import br.qa.andrericardo.appium.core.BaseTest;
import br.qa.andrericardo.appium.page.AccordionPage;
import br.qa.andrericardo.appium.page.MenuPage;

public class AccordionTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private AccordionPage accordion = new AccordionPage();

	@Test
	public void deveInteragirComAccordion() {
		//acessar menu
		menu.acessarAccordion();
		
		//clicar opcao 1
		accordion.selecionarOp1();
		
		//verificar texto opcao 1
		// esperar(1000); 
		// A espera deve ser usada se o dispositivo executar com rapidez a exibi��o do Accordion
		Assert.assertEquals("Esta � a descri��o da op��o 1", accordion.obterValorOp1());
		
	}
}
