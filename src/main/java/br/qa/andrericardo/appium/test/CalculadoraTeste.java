package br.qa.andrericardo.appium.test;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class CalculadoraTeste {

	@Test
	public  void deveSomarDoisValores() throws MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "310091e21fe12400");
	    desiredCapabilities.setCapability("automationName", "uiautomator");
	    desiredCapabilities.setCapability("appPackage", "com.sec.android.app.popupcalculator");
	    desiredCapabilities.setCapability("appActivity", "com.sec.android.app.popupcalculator.Calculator");
	    
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), desiredCapabilities);
	    
	    MobileElement el1 = (MobileElement) driver.findElementByAccessibilityId("5");
	    el1.click();
	    MobileElement el2 = (MobileElement) driver.findElementByAccessibilityId("Mais");
	    el2.click();
	    MobileElement el3 = (MobileElement) driver.findElementByAccessibilityId("3");
	    el3.click();
	    MobileElement el4 = (MobileElement) driver.findElementByAccessibilityId("Igual");
	    el4.click();
	    MobileElement el5 = (MobileElement) driver.findElementByAccessibilityId("Insira o c�lculo.");
	    Assert.assertTrue(el5.getText().contains("=8"));
	    driver.quit();
	}
}
