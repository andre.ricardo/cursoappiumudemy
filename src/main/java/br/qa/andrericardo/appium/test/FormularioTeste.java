package br.qa.andrericardo.appium.test;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.qa.andrericardo.appium.core.BaseTest;
import br.qa.andrericardo.appium.core.DriverFactory;
import br.qa.andrericardo.appium.page.FormularioPage;
import br.qa.andrericardo.appium.page.MenuPage;

public class FormularioTeste extends BaseTest{
	
	private MenuPage menu = new MenuPage();
	private FormularioPage form = new FormularioPage();
	
	@Before
	public void inicializarAppium() throws MalformedURLException {
		menu.acessarFormulario();
	}
	
	@Test
	public  void devePreencherCampoTexto() throws MalformedURLException {
		form.escreverNome("Andr�");
		assertEquals("Andr�", form.obterNome());
	}

	@Test
	public  void deveInteragirCombo() throws MalformedURLException {
		form.selecionarCombo("Nintendo Switch");
	    Assert.assertEquals("Nintendo Switch", form.obterValorCombo());
	}
	
	@Test
	public  void deveInteragirCheckboxSwitch() throws MalformedURLException {
	    Assert.assertFalse(form.isCheckMarcado());
	    Assert.assertTrue(form.isSwitchMarcado());
	    
	    form.clicarCheckbox();
	    form.clicarSwitch();
	    	    
	    Assert.assertTrue(form.isCheckMarcado());
	    Assert.assertFalse(form.isSwitchMarcado());
	    
	}
	
	@Test
	public  void deveRealizarCadastro() throws MalformedURLException {
	    form.escreverNome("Andr� Ricardo");
	    form.clicarCheckbox();
	    form.clicarSwitch();
	    form.selecionarCombo("Nintendo Switch");
	    
	    form.salvar();
	    
	    Assert.assertEquals("Nome: Andr� Ricardo", form.obterNomeCadastrado());
	    Assert.assertEquals("Console: switch", form.obterConsoleCadastrado());
	    Assert.assertTrue(form.obterCheckCadastrado().endsWith("Off"));
	    Assert.assertTrue(form.obterSwitchCadastrado().endsWith("Marcado"));
	}
	
	@Test
	public  void deveRealizarCadastroDemorado() throws MalformedURLException {
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	    form.escreverNome("Andr� Ricardo");
	    
	    form.salvarDemorado();
	    //esperar(3000);
	    WebDriverWait wait  = new WebDriverWait(DriverFactory.getDriver(), 10);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Nome: Andr� Ricardo']")));
	    
	    Assert.assertEquals("Nome: Andr� Ricardo", form.obterNomeCadastrado());    
	}
	
	@Test
	public void deveAlterarData() {
		form.clicarPorTexto("01/01/2000");
		form.clicarPorTexto("20");
		form.clicarPorTexto("OK");
		Assert.assertTrue(form.existeElementoPorTexto("20/2/2000"));
	}
	
	@Test
	public void deveAlterarhora() {
		form.clicarPorTexto("06:00");
		form.selecionarHora("10");
		form.selecionarMinuto("40");
		form.clicarPorTexto("OK");
		Assert.assertTrue(form.existeElementoPorTexto("10:40"));
	}
	
	@Test
	public void deveInteragirComSeekbar() {
		//clicar no seekbar
		form.clicarSeekBar(0.67);
		
		//salvar
		form.salvar();
		
		//obter o valor
		Assert.assertTrue(form.obterSlider().endsWith("67"));
	}
}	
