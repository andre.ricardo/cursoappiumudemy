package br.qa.andrericardo.appium.test;

import org.junit.Assert;
import org.junit.Test;

import br.qa.andrericardo.appium.core.BaseTest;
import br.qa.andrericardo.appium.page.MenuPage;
import br.qa.andrericardo.appium.page.SplashPage;

public class SplashTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private SplashPage page = new SplashPage();
	
	@Test
	public void deveAguardarSplashSumir() {
		//acessar menu splash
		menu.acessarSplash();
		
		//verificar que o splash est� sendo exibido
		page.isTelaSplashVisivel();
		
		//aguardar sa�da do splash
		page.aguardarSplashSumir();
		 
		//verificar que o formulario est� aparecendo
		Assert.assertTrue(page.existeElementoPorTexto("Formul�rio"));		
	}

}
