package br.qa.andrericardo.appium.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.qa.andrericardo.appium.core.BaseTest;
import br.qa.andrericardo.appium.page.CliquesPage;
import br.qa.andrericardo.appium.page.MenuPage;

public class CliquesTeste extends BaseTest {
	
	private MenuPage menu = new MenuPage();
	private CliquesPage clique = new CliquesPage();
	
	@Before
	public void setup() {
		//acessar menu Cliques
		menu.acessarCliques();
	}
	
	@Test
	public void deveRealizarCliqueLongo() {
		//clique logo
		clique.cliqueLongo();
		
		//verificar texto
		Assert.assertEquals("Clique Longo", clique.obterTextoCampo());
	}
	
	@Test
	public void deveRealizarCliqueDuplo() {
		clique.clicarPorTexto("Clique duplo");
		clique.clicarPorTexto("Clique duplo");
		
		Assert.assertEquals("Duplo Clique", clique.obterTextoCampo());
	}

}
