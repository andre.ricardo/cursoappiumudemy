package br.qa.andrericardo.appium.page;

import static br.qa.andrericardo.appium.core.DriverFactory.getDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.touch.TouchActions;

import br.qa.andrericardo.appium.core.BasePage;

public class CliquesPage extends BasePage {
	
	public void cliqueLongo() {
		TouchActions action = new TouchActions(getDriver());
		action.longPress(getDriver().findElement(By.xpath("//*[@text='Clique Longo']")));
		action.perform();
	}
	
	public String obterTextoCampo() {
		return getDriver().findElement(By.xpath("(//android.widget.TextView)[3]")).getText();
	}
}
