package br.qa.andrericardo.appium.core;

import static br.qa.andrericardo.appium.core.DriverFactory.getDriver;

import java.util.List;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;


public class BasePage {

public void escrever(By by, String texto) {
		
		getDriver().findElement(by).sendKeys(texto); 
	}
	
	public String obterTexto(By by) {
		
		return getDriver().findElement(by).getText();
	}
	
	public void clicar(By by) {
		getDriver().findElement(by).click();
	}
	
	public void clicarPorTexto(String texto) {
		clicar(By.xpath("//*[@text='"+texto+"']"));
	}

	public void selecionarCombo(By by, String valor) {
		
	    getDriver().findElement(by).click();
	    clicarPorTexto(valor);
	}
	
	public boolean isCheckedMarcado(By by) {
		return getDriver().findElement(by).getAttribute("checked").equals("true");
	}
	
	public boolean existeElementoPorTexto(String texto) {
		List<MobileElement> elementos = getDriver().findElements(By.xpath("//*[@text='"+texto+"']"));
		return elementos.size() > 0;
	}
	
	public boolean existeElementoPorTexto(String str1, String str2, String str3) {
		MobileElement el1 =  getDriver().findElement(By.xpath("//*[@text='"+str1+"']"));
		MobileElement el2 =  getDriver().findElement(By.xpath("//*[@text='"+str2+"']"));
		MobileElement el3 =  getDriver().findElement(By.xpath("//*[@text='"+str3+"']"));
		return el1.isEnabled() & el2.isEnabled() & el3.isEnabled() ;
	}
	
	public void tap(int x, int y) {
		//new TouchAction(getDriver()).tap(x, y).perform();
		TouchAction touchAction = new TouchAction(DriverFactory.getDriver());
		touchAction.tap(new PointOption().withCoordinates(x, y)).perform();
	}
}
